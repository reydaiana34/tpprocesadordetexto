/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;
import javax.swing.*;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;


public class ProcesadorTexto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Marco word = new Marco();
    }

}

class Marco extends JFrame {
    Panel panel;
    public Marco() {
        setSize(1200, 700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Procesador de Texto");
        setMinimumSize(new Dimension(800, 600));
        addWindowListener(new VentanaEventos());
        panel = new Panel();
        add(panel);
        setVisible(true);

    }
    class VentanaEventos implements WindowListener{
        @Override
        public void windowOpened(WindowEvent e) {
        }
        @Override
        public void windowClosing(WindowEvent e) {
            panel.guardarArchivo();
        }
        @Override
        public void windowClosed(WindowEvent e) {
        }
        @Override
        public void windowIconified(WindowEvent e) {
        }
        @Override
        public void windowDeiconified(WindowEvent e) {
        }
        @Override
        public void windowActivated(WindowEvent e) {
        }
        @Override
        public void windowDeactivated(WindowEvent e) {
        }
    }
}
class Panel extends JPanel {

    private static JTextPane miarea;

    JMenu fuente, estilo, tamanio, archivo;

    Font letras;
    
    private static String nombre = "";
    
    private static String directorio = "";
    
    
    public Panel() {
        //Creamos el layout
        setLayout(new BorderLayout());

        //Creamos el panel del menú
        JPanel panelmenu = new JPanel();

        //Creamos el menú en sí
        JMenuBar menubar = new JMenuBar();

        //Creamos los elementos del menú
        fuente = new JMenu("Fuente");
        tamanio = new JMenu("Tamaño");
        estilo = new JMenu("Estilo");
        archivo = new JMenu("Archivo");

        //Agregamos los componentes del menú al menú
        menubar.add(fuente);
        menubar.add(tamanio);
        menubar.add(estilo);
        menubar.add(archivo);

        //Agregamos las opciones de Fuente
        conf_menu("Arial", "fuente", "Arial", 9, 10);
        conf_menu("Verdana", "fuente", "Verdana", 9, 10);
        conf_menu("Courier", "fuente", "Courier", 9, 10);
        conf_menu("Roboto", "fuente", "Roboto", 9, 10);
        conf_menu("Garamond", "fuente", "Garamond", 9, 10);
        conf_menu("Comic Sans MS", "fuente", "Comic Sans MS", 9, 10);

        //Agregamos las opciones de estilo
        conf_menu("Negrita", "estilo", "", Font.BOLD, 1);
        conf_menu("Cursiva", "estilo", "", Font.ITALIC, 1);

        //Agregamos las opciones de tamaño
        conf_menu("12", "tamanio", "", 9, 12);
        conf_menu("14", "tamanio", "", 9, 14);
        conf_menu("16", "tamanio", "", 9, 16);
        conf_menu("18", "tamanio", "", 9, 18);
        conf_menu("20", "tamanio", "", 9, 20);
        conf_menu("22", "tamanio", "", 9, 22);
        conf_menu("24", "tamanio", "", 9, 24);
        conf_menu("26", "tamanio", "", 9, 26);
        conf_menu("28", "tamanio", "", 9, 28);

        //Agregamos las opciones de Archivo
        conf_menu("Abrir", "archivo", "", 1, 1);
        conf_menu("Guardar", "archivo", "", 1, 1);

        //----------------------------------------------------------------------
        //Agregamos el menú al panel
        panelmenu.add(menubar);
        add(menubar, BorderLayout.NORTH);

        //Agregando el panel de texto
        miarea = new JTextPane();
        JScrollPane scrollPane = new JScrollPane(miarea);
        add(scrollPane, BorderLayout.CENTER);

    }

    public void conf_menu(String n, String menu, String fuenteletra, int estilos, int tamano) {
        JMenuItem menu_element = new JMenuItem(n);

        if (menu == "fuente") {
            fuente.add(menu_element);
            if (fuenteletra == "Arial") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Arial"));
            } else if (fuenteletra == "Verdana") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Verdana"));
            } else if (fuenteletra == "Courier") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Courier"));
            } else if (fuenteletra == "Roboto") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Roboto"));
            } else if (fuenteletra == "Garamond") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Garamond"));
            } else if (fuenteletra == "Comic Sans MS") {
                menu_element.addActionListener(new StyledEditorKit.FontFamilyAction("fuente", "Comic Sans MS"));
            }
        } else if (menu == "tamanio") {
            tamanio.add(menu_element);
            menu_element.addActionListener(new StyledEditorKit.FontSizeAction("tam", tamano));

        } else if (menu == "estilo") {
            estilo.add(menu_element);
            if (estilos == Font.BOLD) {
                menu_element.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                menu_element.addActionListener(new StyledEditorKit.BoldAction());
            } else if (estilos == Font.ITALIC) {
                menu_element.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                menu_element.addActionListener(new StyledEditorKit.ItalicAction());
            }

        } else if (menu == "archivo") {
            if (n == "Guardar") {
                menu_element.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));
                menu_element.addActionListener(new saveFile());
                archivo.add(menu_element);
            } else if (n == "Abrir") {
                menu_element.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
                menu_element.addActionListener(new openFile());
                archivo.add(menu_element);
            }
        } else {
            System.out.println("Error en la carga del item del menú");
        }
    }

    private class openFile implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
           cargarArchivo();
        }
    }

    private class saveFile implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            guardarArchivo();
        }
    }
    
    public static void guardarArchivo(){
        try {
            JFrame dialog = new JFrame();
            FileDialog fd = new FileDialog(dialog,"Guardar archivo",FileDialog.SAVE);
            fd.setVisible(true);

            if (fd.getFile() != null){
                nombre = fd.getFile();
                directorio = fd.getDirectory();
            }
            ObjectOutputStream save= new ObjectOutputStream(new FileOutputStream(directorio+nombre));
            save.writeObject(miarea.getText());
            save.close();
        }catch (IOException ex){
            JOptionPane.showConfirmDialog(null, "El archivo no se guardó", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public static void cargarArchivo(){
        try{
            JFrame dialog = new JFrame();
            FileDialog fd = new FileDialog(dialog,"Abrir archivo",FileDialog.LOAD);
            fd.setVisible(true);

            if (fd.getFile()!=null){
                nombre = fd.getFile();
                directorio = fd.getDirectory();
            }
            String path =directorio+nombre;

            ObjectInputStream leerArchivos = new ObjectInputStream(new FileInputStream(path));
            miarea.setText((String) leerArchivos.readObject());
            leerArchivos.close();
        } catch (Exception ex){
            JOptionPane.showConfirmDialog(null, "No se encontro el archivo", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }
}

